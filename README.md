# OpenML dataset: covertype

https://www.openml.org/d/44121

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "classification on numerical features" benchmark. Original description: 
 
**Author**: Jock A. Blackard, Dr. Denis J. Dean, Dr. Charles W. Anderson   
**Source**: [LibSVM repository](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/) - 2013-11-14  
**Please cite**: For the binarization: R. Collobert, S. Bengio, and Y. Bengio. A parallel mixture of SVMs for very large scale problems. Neural Computation, 14(05):1105-1114, 2002.

This is the famous covertype dataset in its binary version, retrieved 2013-11-13 from the libSVM site (called covtype.binary there). Additional to the preprocessing done there (see LibSVM site for details), this dataset was created as  follows:
-load covertpype dataset, unscaled.
-normalize each file columnwise according to the following rules:
-If a column only contains one value (constant feature), it will set to zero and thus removed by sparsity.
-If a column contains two values (binary feature), the value occuring more often will be set to zero, the other to one.
-If a column contains more than two values (multinary/real feature), the column is divided by its std deviation.
-duplicate lines were finally removed.

Preprocessing: Transform from multiclass into binary class.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44121) of an [OpenML dataset](https://www.openml.org/d/44121). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44121/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44121/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44121/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

